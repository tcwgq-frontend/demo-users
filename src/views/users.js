/* target: 2.7, extensions: [".vue"], lib: "vue", jsxSlots: false, strictTemplates: false, skipTemplateCodegen: false, nativeTags: ["html","body","base","head","link","meta","style","title","address","article","aside","footer","header","hgroup","h1","h2","h3","h4","h5","h6","nav","section","div","dd","dl","dt","figcaption","figure","picture","hr","img","li","main","ol","p","pre","ul","a","b","abbr","bdi","bdo","br","cite","code","data","dfn","em","i","kbd","mark","q","rp","rt","ruby","s","samp","small","span","strong","sub","sup","time","u","var","wbr","area","audio","map","track","video","embed","object","param","source","canvas","script","noscript","del","ins","caption","col","colgroup","table","thead","tbody","td","th","tr","button","datalist","fieldset","form","input","label","legend","meter","optgroup","option","output","progress","select","textarea","details","dialog","menu","summary","template","blockquote","iframe","tfoot","svg","animate","animateMotion","animateTransform","circle","clipPath","color-profile","defs","desc","discard","ellipse","feBlend","feColorMatrix","feComponentTransfer","feComposite","feConvolveMatrix","feDiffuseLighting","feDisplacementMap","feDistanceLight","feDropShadow","feFlood","feFuncA","feFuncB","feFuncG","feFuncR","feGaussianBlur","feImage","feMerge","feMergeNode","feMorphology","feOffset","fePointLight","feSpecularLighting","feSpotLight","feTile","feTurbulence","filter","foreignObject","g","hatch","hatchpath","image","line","linearGradient","marker","mask","mesh","meshgradient","meshpatch","meshrow","metadata","mpath","path","pattern","polygon","polyline","radialGradient","rect","set","solidcolor","stop","switch","symbol","text","textPath","tspan","unknown","use","view","slot","component"], dataAttributes: [], htmlAttributes: ["aria-*"], optionsWrapper: ["(await import('vue')).defineComponent(",")"], macros: {"defineProps":["defineProps"],"defineSlots":["defineSlots"],"defineEmits":["defineEmits"],"defineExpose":["defineExpose"],"withDefaults":["withDefaults"]}, plugins: [], hooks: [], experimentalDefinePropProposal: false, experimentalAdditionalLanguageModules: [], experimentalResolveStyleCssClasses: "scoped", experimentalModelPropName: {"":{"input":true},"value":{"input":{"type":"text"},"textarea":true,"select":true}}, experimentalUseElementAccessInTemplate: false */
export default (await import('vue')).defineComponent({
  name: 'MyUsers',
  created() {
    this.getUserList()
  },
  data() {
    const checkAge = (rule, value, callback) => {
      if (!value) {
        return callback(new Error('年龄不能为空'))
      }
      setTimeout(() => {
        if (!Number.isInteger(value)) {
          callback(new Error('请输入数字值'))
        } else {
          if (value < 0 || value > 100) {
            callback(new Error('年龄只能在1到100之间'))
          } else {
            callback()
          }
        }
      }, 1000)
    }
    const checkBirthday = (rule, value, callback) => {
      if (!value) {
        return callback(new Error('生日不能为空'))
      }
    }
    return {
      pageNo: 1,
      pageSize: 10,
      userList: [],
      dialogVisible: false,
      currentPage: 1,
      total: 0,
      isEdit: false,
      title: '新增',
      form: {
        name: '',
        sex: '男',
        age: '',
        birthday: ''
      },
      rules: {
        name: [
          { required: true, message: '请输入姓名', trigger: 'blur' },
          { min: 2, max: 10, message: '长度在2到10个字符', trigger: 'blur' }
        ],
        age: [
          { required: true, message: '请输入年龄', trigger: 'blur' },
          { type: 'number', message: '年龄必须为数字值' },
          { trigger: 'blur', validator: checkAge }
        ],
        // 这里如果不采用validator形式，会报错
        birthday: [{ type: 'date', required: true, message: '请选择生日', trigger: 'blur', validator: checkBirthday }]
      }
    }
  },
  methods: {
    async getUserList() {
      const { data: res } = await this.$http.post('/api/author/page', {
        pageNo: this.pageNo,
        pageSize: this.pageSize
      })
      // console.log(res)
      this.userList = res.data.list
      this.total = res.data.total
    },
    handleSizeChange(pageSize) {
      this.pageSize = pageSize
      this.getUserList()
    },
    handleCurrentChange(pageNo) {
      this.pageNo = pageNo
      this.getUserList()
    },
    handleClose() {
      this.dialogVisible = false
      this.$refs.form.resetFields()
      this.$refs.form.clearValidate()
      this.isEdit = false
      this.title = '新增'
    },
    handleCancel() {
      this.dialogVisible = false
      this.$refs.form.resetFields()
      this.$refs.form.clearValidate()
      this.isEdit = false
      this.title = '新增'
    },
    async add() {
      let add = true
      this.$refs.form.validate((result, object) => {
        if (!result) {
          add = false
        }
      })
      if (add) {
        let op = '添加'
        let url = '/api/author/add'
        if (this.isEdit) {
          op = '编辑'
          url = '/api/author/edit'
        }
        const { data: res } = await this.$http.post(url, this.form)
        if (res.code === 0) {
          this.$message({
            message: `${op}成功！`,
            type: 'success'
          })
          this.$refs.form.resetFields()
          this.$refs.form.clearValidate()
          this.dialogVisible = false
          this.isEdit = false
          this.title = '新增'
          this.getUserList()
        } else {
          const message = `${op}失败，` + res.message
          this.$message({
            message: message,
            type: 'error'
          })
          this.$refs.form.resetFields()
          this.$refs.form.clearValidate()
          this.dialogVisible = false
        }
      }
    },
    async handleEdit(index, row) {
      const { data: res } = await this.$http.get('/api/author/get/' + row.id)
      this.form.id = res.data.id
      this.form.name = res.data.name
      this.form.sex = res.data.sex
      this.form.age = res.data.age
      this.form.birthday = res.data.birthday
      this.dialogVisible = true
      this.isEdit = true
      this.title = '编辑'
    },
    async handleDelete(index, row) {
      this.$confirm('此操作将永久删除该用户, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
        .then(async () => {
          const { data: res } = await this.$http.delete('/api/author/delete/' + row.id)
          if (res.code === 0) {
            this.$message({
              type: 'success',
              message: '删除成功!'
            })
            this.getUserList()
          } else {
            this.$message({
              type: 'error',
              message: '删除失败!'
            })
          }
        })
        .catch(() => {
          this.$message({
            type: 'info',
            message: '已取消删除'
          })
        })
    }
  }
})
